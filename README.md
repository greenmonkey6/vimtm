## Usage

Just execute the following.

```sh
$ vim -S TM.vim TM.vim
```

You can now use F1 to simulate a single step or F2 to simulate the whole simulation. Keybindings can be changed in the last fold.

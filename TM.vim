" 101
" H
"Current State: q0
"
"Q  Γ    δ
"q0 B (q1,#,R)
"q0 0 (q0,0,R)
"q0 1 (q0,1,R)
"q1 B (q2,1,R)
"q1 0 (q5,0,R)
"q1 1 (q1,1,R)
"q2 B (q2,0,N)
"q2 0 (q3,0,R)
"q3 B (q4,1,R)
"q3 0 (qF,0,R)
"q4 B (q1,0,L)
"q5 B (q5,1,N)
"q5 1 (q6,1,R)
"q6 B (q3,0,N)

" HOW TO EXECUTE
"
" A step of the TM can then be executed using F1 (or @r). The whole
" computation can be executed using F2 (or @e).

" SYNTAX OF THE TM
"
" There needs to be a space at the beginning of the tape and head line.
" Undefined behavior when the TM runs into a negative position.
" Possible values:
" State:         Any (vim) word
" Letter:        Any letter, B is Blank
" Movement:      L, N or R
" Table Spacing: Any whitespace (no alignment required)

" PROGRAM

nnoremap <F1> @r
nnoremap <F2> @e

" Find matching line in table
let @f = '/s\s\s*h\s\s*(..*,..*,..*)0'
" Get next state, head and movement from current table line
let @g = 'f(l"syiwf,l"hylf,l"myl'
" Apply current registers
let @a = ':10jfHk"hphx:20fHhCLNR0:s/m/H:s/[^H" ]/ /g:3$ciw"sp'
" Read current symbol from tape and current state from below
let @t = ':10jfHk"hyl:3$"syiw'
" Make consistent
let @c = ':2yyP:s/./B/gk0"0y$jR0kdd'
" Run one step of the TM
let @r = '@t@f@g@a@c:nohl:20fHk'
" Do the whole run of the TM
let @e = '@r@e'

" Print current registers
let @p = 'oHead:  "hpState: "spMove:  "mp'

$-40,$-37 fold
$-35,$-27 fold
$-25,$ fold
